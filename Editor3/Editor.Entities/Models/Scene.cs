﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Editor.Entities.Models
{
    public class Scene
    {
        [Key]
        public int SceneId { get; set; }

        //public List<Anatomic> Anatomics { get; set; }

        //public List<Vendor> Vendors { get; set; }

        public bool HasVendor { get; set; }

        public DateTime Time { get; set; }

    }
}

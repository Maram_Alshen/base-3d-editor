﻿using Editor.Data.Context;
using Editor.Data.Interfaces;
using Editor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Editor.Data.Repositories
{
    public class GNoteRepo : IGNote
    {
        private readonly AppDbContext _appContext;

        //Constracter
        public GNoteRepo(AppDbContext appDbContext)
        {
            _appContext = appDbContext;
        }
        //........................................
        //Add Generate New Note
        public void GeneratNote(GNote gNote)
        {
            _appContext.GNotes.Add(gNote);
            _appContext.SaveChanges();
        }
        //........................................
        //Get All Note
        public IEnumerable<GNote> GetAllGNotes()
        {
            return _appContext.GNotes;
        }
        //........................................
        //Get Note By Id
        public GNote GetGNoteById(int gNotId)
        {
            return _appContext.GNotes.FirstOrDefault(g => g.GNoteId == gNotId);
        }
    }
}
﻿using Editor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Editor.Data.Interfaces
{

    public interface IGNote
    {
        void GeneratNote(GNote gNote);
        IEnumerable<GNote> GetAllGNotes();
        GNote GetGNoteById(int gNotId);
    }
}
